function openForm() {
  document.getElementById("myForm").style.display = "block";
}

function closeForm() {
  document.getElementById("myForm").style.display = "none";
}

function myfunction() {
	document.getElementById("one").innerHTML = "Akoya Pearls & Diamonds Linear Earrings";
}

function myCart() {
		alert("Your item was added to cart")
}

let carts = document.querySelectorAll('.add-cart');
let cartss = document.querySelectorAll('.ad-cart');

let products = [
	{
	    name: "Akoya Pearl Ring",
	    tag: "Ring1",
	    price: 345,
	    inCart: 0
	},
	{
	    name: "Zoe Chicco Mixed Diamond Ring",
	    tag: "Ring2",
	    price: 295,
	    inCart: 0
	},
	{
	    name: "Double-G Diamond Stack Ring",
	    tag: "Ring3",
	    price: 355,
	    inCart: 0
	},
	{
	    name: "Half Eternity Diamond Baguette",
	    tag: "Ring4",
	    price: 265,
	    inCart: 0
	},
	{
	    name: "Pandora Princess Wishbone Cubic Zirconia Ring",
	    tag: "Ring5",
	    price: 486,
	    inCart: 0
	},
	{
	    name: "Ouroboros Diamond Pave Snake Ring",
	    tag: "Ring6",
	    price: 645,
	    inCart: 0
	},
	{
	    name: "Love Knot Diamond Ring",
	    tag: "Ring7",
	    price: 666,
	    inCart: 0
	},
	{
	    name: "Infinity Twist Diamond Ring In Rose Gold",
	    tag: "Ring8",
	    price: 565,
	    inCart: 0
	},
	{
		name: "Akoya Pearls & Diamonds Linear Earrings",
		tag: "eRing1",
		price: 456,
		inCart: 0
	},
	{
	    name: "Akoya Pearls Drop Earrings",
	    tag: "eRing2",
	    price: 499,
	    inCart: 0
	},
	{
	    name: "Diamond & Gold Earings",
	    tag: "eRing3",
	    price: 450,
	    inCart: 0
	},
	{
	    name: "0.50ct tw Diamond & Platinum Stud Earrings",
	    tag: "eRing4",
	    price: 445,
	    inCart: 0
	},
	{
	    name: "Zoe Chicco Diamond Circle Stud Earrings",
	    tag: "eRing5",
	    price: 345,
	    inCart: 0
	},
	{
	    name: "Simulated Diamond Stud Earrings",
	    tag: "eRing6",
	    price: 768,
	    inCart: 0
	},
	{
	    name: "Gold Tubular Hoop Earrings",
	    tag: "eRing7",
	    price: 300,
	    inCart: 0
	},
	{
	    name: "Single Diamond Bar & Chain Earring",
	    tag: "eRing8",
	    price: 239,
	    inCart: 0
	}
];

let productss = [
     {
		name: "Akoya Pearls & Diamonds Linear Earrings",
		tag: "eRing1",
		price: 456,
		inCart: 0
	},
	{
	    name: "Akoya Pearls Drop Earrings",
	    tag: "eRing2",
	    price: 499,
	    inCart: 0
	},
	{
	    name: "Diamond & Gold Earings",
	    tag: "eRing3",
	    price: 450,
	    inCart: 0
	},
	{
	    name: "0.50ct tw Diamond & Platinum Stud Earrings",
	    tag: "eRing4",
	    price: 445,
	    inCart: 0
	},
	{
	    name: "Zoe Chicco Diamond Circle Stud Earrings",
	    tag: "eRing5",
	    price: 345,
	    inCart: 0
	},
	{
	    name: "Simulated Diamond Stud Earrings",
	    tag: "eRing6",
	    price: 768,
	    inCart: 0
	},
	{
	    name: "Gold Tubular Hoop Earrings",
	    tag: "eRing7",
	    price: 300,
	    inCart: 0
	},
	{
	    name: "Single Diamond Bar & Chain Earring",
	    tag: "eRing8",
	    price: 239,
	    inCart: 0
	}
];

for(let i=0; i<carts.length; i++)
{
	carts[i].addEventListener('click',() =>{
		cartNumbers(products[i]);
		totalCost(products[i]);
	})
}

for(let k=0; k<cartss.length; k++)
{
	cartss[k].addEventListener('click',() =>{
		cartNumbers(productss[k]);
		totalCost(productss[k]);
	})
}

function onLoadCartNumbers() {
    let productNumbers = localStorage.getItem('cartNumbers');
    if( productNumbers ) {
        document.querySelector('.cart span').textContent = productNumbers;
    }
}

function cartNumbers(product, action, productsss) {
    let productNumbers = localStorage.getItem('cartNumbers');
    productNumbers = parseInt(productNumbers);

    let cartItems = localStorage.getItem('productsInCart');
    cartItems = JSON.parse(cartItems);

    if( action ) {
        localStorage.setItem("cartNumbers", productNumbers - 1);
        document.querySelector('.cart span').textContent = productNumbers - 1;
        console.log("action running");
    } else if( productNumbers ) {
        localStorage.setItem("cartNumbers", productNumbers + 1);
        document.querySelector('.cart span').textContent = productNumbers + 1;
    } else {
        localStorage.setItem("cartNumbers", 1);
        document.querySelector('.cart span').textContent = 1;
    }
    setItems(product);
}

function setItems(product, productsss) {
    // let productNumbers = localStorage.getItem('cartNumbers');
    // productNumbers = parseInt(productNumbers);
    let cartItems = localStorage.getItem('productsInCart');
    cartItems = JSON.parse(cartItems);

    if(cartItems != null) {
        let currentProduct = product.tag;
    
        if( cartItems[currentProduct] == undefined ) {
            cartItems = {
                ...cartItems,
                [currentProduct]: product
            }
        } 
        cartItems[currentProduct].inCart += 1;

    } else {
        product.inCart = 1;
        cartItems = { 
            [product.tag]: product
        };
    }

    localStorage.setItem('productsInCart', JSON.stringify(cartItems));
}

function totalCost( product, action, productsss ) {
    let cart = localStorage.getItem("totalCost");

    if( action) {
        cart = parseInt(cart);

        localStorage.setItem("totalCost", cart - product.price);
    } else if(cart != null) {
        
        cart = parseInt(cart);
        localStorage.setItem("totalCost", cart + product.price);
    
    } else {
        localStorage.setItem("totalCost", product.price);
    }
}

let delete_buttons = document.getElementsByClassName('uk-button-danger');

function displayCart() {
    let cartItems = localStorage.getItem('productsInCart');
    cartItems = JSON.parse(cartItems);

    let cart = localStorage.getItem("totalCost");
    cart = parseInt(cart);

    let productContainer = document.querySelector('.products');
    
    if( cartItems && productContainer ) {
        productContainer.innerHTML = '';
        Object.values(cartItems).map( (item, index) => {
            productContainer.innerHTML += 
            `<div class="product"><ion-icon name="close-circle"></ion-icon><img src="./images/${item.tag}.jpg" />
                <span class="sm-hide">${item.name}</span>
            </div>
            <div class="pricing sm-hide">RM${item.price}.00</div>
            <div class="quantity">
                <ion-icon class="decrease " name="arrow-dropleft-circle"></ion-icon>
                    <span>${item.inCart}</span>
                <ion-icon class="increase" name="arrow-dropright-circle"></ion-icon>   
            </div>
            <div class="total">RM${item.inCart * item.price}.00</div>`
        });

        productContainer.innerHTML += `
            <div class="basketTotalContainer">
                <h4 class="basketTotalTitle">Basket Total</h4>
                <h4 class="basketTotal">RM${cart}.00</h4>
            </div>`
            
        deleteButtons();
        manageQuantity();
    }
}

function manageQuantity() {
    let decreaseButtons = document.querySelectorAll('.decrease');
    let increaseButtons = document.querySelectorAll('.increase');
    let currentQuantity = 0;
    let currentProduct = '';
    let cartItems = localStorage.getItem('productsInCart');
    cartItems = JSON.parse(cartItems);

    for(let i=0; i < increaseButtons.length; i++) {
        decreaseButtons[i].addEventListener('click', () => {
            console.log(cartItems);
            currentQuantity = decreaseButtons[i].parentElement.querySelector('span').textContent;
            console.log(currentQuantity);
            currentProduct = decreaseButtons[i].parentElement.previousElementSibling.previousElementSibling.querySelector('span').textContent.toLocaleLowerCase().replace(/ /g,'').trim();
            console.log(currentProduct);

            if( cartItems[currentProduct].inCart > 1 ) {
                cartItems[currentProduct].inCart -= 1;
                cartNumbers(cartItems[currentProduct], "decrease");
                totalCost(cartItems[currentProduct], "decrease");
                localStorage.setItem('productsInCart', JSON.stringify(cartItems));
                displayCart();
            }
        });

        increaseButtons[i].addEventListener('click', () => {
            console.log(cartItems);
            currentQuantity = increaseButtons[i].parentElement.querySelector('span').textContent;
            console.log(currentQuantity);
            currentProduct = increaseButtons[i].parentElement.previousElementSibling.previousElementSibling.querySelector('span').textContent.toLocaleLowerCase().replace(/ /g,'').trim();
            console.log(currentProduct);

            cartItems[currentProduct].inCart += 1;
            cartNumbers(cartItems[currentProduct]);
            totalCost(cartItems[currentProduct]);
            localStorage.setItem('productsInCart', JSON.stringify(cartItems));
            displayCart();
        });
    }
}

function deleteButtons(product, productsss) {
    let deleteButtons = document.querySelectorAll('.product ion-icon');
    let productNumbers = localStorage.getItem('cartNumbers');
    let cartCost = localStorage.getItem("totalCost");
    let cartItems = localStorage.getItem('productsInCart');
    cartItems = JSON.parse(cartItems);
    let productName;
    console.log(cartItems);

    for(let i=0; i < deleteButtons.length; i++) {
        deleteButtons[i].addEventListener('click', () => {
            productName = deleteButtons[i].parentElement.textContent.toLocaleLowerCase().replace(/ /g,'').trim();
           
            localStorage.setItem('cartNumbers', productNumber - cartItems[productName].inCart);
            localStorage.setItem('totalCost', cartCost - ( cartItems[productName].price * cartItems[productName].inCart));

            delete cartItems[productName];
            localStorage.setItem('productsInCart', JSON.stringify(cartItems));

            displayCart();
            onLoadCartNumbers();
        });
    }
}

onLoadCartNumbers();
displayCart();
